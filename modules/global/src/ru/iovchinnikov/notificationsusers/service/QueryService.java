package ru.iovchinnikov.notificationsusers.service;


import com.haulmont.cuba.core.global.LoadContext;
import ru.iovchinnikov.notificationsusers.entity.ExtUser;

public interface QueryService {
    String NAME = "notificationsusers_QueryService";

    void setExtUserLoadContext(LoadContext<ExtUser> extUserLoadContext);

    LoadContext getLoadContext();
}