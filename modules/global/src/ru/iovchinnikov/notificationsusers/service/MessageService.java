package ru.iovchinnikov.notificationsusers.service;

public interface MessageService {
    String NAME = "notificationsusers_MessageService";

    void send(String senderLogin, String receiverLogin, String subject, String text);

}