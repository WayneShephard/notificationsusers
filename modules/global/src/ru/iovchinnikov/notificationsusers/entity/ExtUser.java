package ru.iovchinnikov.notificationsusers.entity;

import javax.persistence.Entity;
import com.haulmont.cuba.core.entity.annotation.Extends;
import javax.persistence.Column;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.core.entity.FileDescriptor;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.UUID;

@Extends(User.class)
@Entity(name = "notificationsusers$ExtUser")
public class ExtUser extends User {
    private static final long serialVersionUID = -1971213726917876951L;

    @Column(name = "COMPANY_REF")
    protected String companyRef;

    @Column(name = "COMPANY_ID")
    protected UUID companyId;

    @Column(name = "PHONE", length = 50)
    protected String phone;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FILE_ID")
    protected FileDescriptor file;

    public void setCompanyId(UUID companyId) {
        this.companyId = companyId;
    }

    public UUID getCompanyId() {
        return companyId;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setFile(FileDescriptor file) {
        this.file = file;
    }

    public FileDescriptor getFile() {
        return file;
    }


    public void setCompanyRef(String companyRef) {
        this.companyRef = companyRef;
    }

    public String getCompanyRef() {
        return companyRef;
    }


}