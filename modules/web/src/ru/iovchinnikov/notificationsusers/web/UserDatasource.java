package ru.iovchinnikov.notificationsusers.web;

import com.haulmont.chile.core.model.MetaClass;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.data.impl.CustomCollectionDatasource;
import ru.iovchinnikov.notificationsusers.entity.ExtUser;
import ru.iovchinnikov.notificationsusers.service.QueryService;

import java.util.*;

/**
 * Created by Stepanov_ME on 24.04.2018.
 */
public class UserDatasource extends CustomCollectionDatasource<ExtUser, UUID> {

    QueryService queryService= AppBeans.get(QueryService.NAME);
    DataManager dataManager= AppBeans.get(DataManager.NAME);

    @Override
    protected Collection getEntities(Map params) {
            LoadContext loadContext = queryService.getLoadContext();
            if (loadContext != null) {
                List list=dataManager.loadList(loadContext);
                if(list==null){
                    return new ArrayList();
                }
                else{
                    return list;
                }
            } else {
                return new ArrayList();
            }

    }

    @Override
    public Collection getItems() {
        LoadContext loadContext = queryService.getLoadContext();
        if (loadContext != null) {
            List list=dataManager.loadList(loadContext);
            if(list==null){
                return new ArrayList();
            }
            else {
                return list;
            }
        } else {
            return new ArrayList();
        }

    }

    @Override
    public MetaClass getMetaClass() {
        return metadata.getClass(ExtUser.class);
    }
}
