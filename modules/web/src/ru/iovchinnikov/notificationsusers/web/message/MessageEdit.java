package ru.iovchinnikov.notificationsusers.web.message;

import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.security.global.UserSession;
import ru.iovchinnikov.notificationsusers.entity.ExtUser;
import ru.iovchinnikov.notificationsusers.entity.Message;
import ru.iovchinnikov.notificationsusers.entity.MessageMeta;
import ru.iovchinnikov.notificationsusers.entity.MessageText;
import ru.iovchinnikov.notificationsusers.service.QueryService;

import javax.inject.Inject;
import java.util.*;

public class MessageEdit extends AbstractEditor<Message> {

    @Inject private Metadata metadata;
    @Inject private UserSession userSession;
    @Inject private FieldGroup fieldGroup;
    @Inject private RichTextArea messageTextTextArea;
    @Inject private LookupField companies;
    @Inject private CollectionDatasource usersDs;
    @Inject private LookupField recipient;
    @Inject private QueryService queryService;

    LoadContext<ExtUser> extUserLoadContext=LoadContext.create(ExtUser.class);

    @Override
    protected void initNewItem(Message item) {
        if (item.getMessageText() == null)
            item.setMessageText(metadata.create(MessageText.class));
        if (item.getMeta() == null)
            item.setMeta(metadata.create(MessageMeta.class));
        item.setSender(userSession.getUser());
        item.setIsSystem(false);
        fieldGroup.setEditable(true);
        messageTextTextArea.setEditable(true);
    }

    @Override
    public void ready() {
        List<String> list;
        Set<String> set = new HashSet<>();
        LoadContext.Query queryTemp=LoadContext.createQuery("select e from notificationsusers$ExtUser e " +
                "where e.login <> :session$userLogin " +
                "and e.login <> 'anonymous'");
        queryTemp.setParameter("session$userLogin",userSession.getAttribute("userLogin"));
        extUserLoadContext.setQuery(queryTemp);
        queryService.setExtUserLoadContext(extUserLoadContext);
        recipient.setValue(null);
        usersDs.refresh();

        for (Object u : usersDs.getItems()) {
            if (((ExtUser)u).getCompanyRef() != null && !"".equals(((ExtUser)u).getCompanyRef()))
                set.add(((ExtUser)u).getCompanyRef());
        }
        set.add(((ExtUser) userSession.getUser()).getCompanyRef());
        list = new ArrayList<>(set);
        companies.setOptionsList(list);
        companies.addValueChangeListener(e -> {
            String company = (companies.getValue() != null) ? companies.getValue() : "";
            LoadContext.Query query;
            switch (company) {
                case "":
                    query=LoadContext.createQuery("select e from notificationsusers$ExtUser e " +
                            "where e.login <> :session$userLogin " +
                            "and e.login <> 'anonymous'");
                    query.setParameter("session$userLogin",userSession.getAttribute("userLogin"));
                    extUserLoadContext.setQuery(query);
                    queryService.setExtUserLoadContext(extUserLoadContext);
                    break;
                default:
                    query=LoadContext.createQuery("select e from notificationsusers$ExtUser e " +
                            "where e.login <> :session$userLogin " +
                            "and e.login <> 'anonymous' " +
                            "and e.companyRef =:company ");
                    query.setParameter("company",company);
                    query.setParameter("session$userLogin",userSession.getAttribute("userLogin"));
                    extUserLoadContext.setQuery(query);
                    queryService.setExtUserLoadContext(extUserLoadContext);
            }
            recipient.setValue(null);
            usersDs.refresh();
        });
        super.ready();
    }

}