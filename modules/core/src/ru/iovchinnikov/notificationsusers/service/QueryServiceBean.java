package ru.iovchinnikov.notificationsusers.service;

import com.haulmont.cuba.core.global.LoadContext;
import org.springframework.stereotype.Service;
import ru.iovchinnikov.notificationsusers.entity.ExtUser;

@Service(QueryService.NAME)
public class QueryServiceBean implements QueryService {
    LoadContext<ExtUser> extUserLoadContext;

    @Override
    public void setExtUserLoadContext(LoadContext<ExtUser> extUserLoadContext){
        this.extUserLoadContext=extUserLoadContext;
    }

    @Override
    public LoadContext getLoadContext(){
        return extUserLoadContext;
    }
}