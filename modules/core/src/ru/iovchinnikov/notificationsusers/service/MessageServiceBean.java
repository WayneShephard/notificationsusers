package ru.iovchinnikov.notificationsusers.service;

import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;
import ru.iovchinnikov.notificationsusers.entity.Message;
import ru.iovchinnikov.notificationsusers.entity.MessageMeta;
import ru.iovchinnikov.notificationsusers.entity.MessageText;

import javax.inject.Inject;
import java.util.Objects;

@Service(MessageService.NAME)
public class MessageServiceBean implements MessageService {

    @Inject
    private DataManager dataManager;
    @Inject
    private Metadata metadata;

    @Override
    public void send(String sender, String receiver, String subject, String text) {
        Message newMsg = metadata.create(Message.class);
        newMsg.setMessageText(metadata.create(MessageText.class));
        newMsg.setMeta(metadata.create(MessageMeta.class));
        LoadContext<User> loadContext = LoadContext.create(User.class)
                .setQuery(LoadContext.createQuery("select p from sec$User p where p.login = :username")
                        .setParameter("username", receiver));
        User receiverUser = dataManager.load(loadContext);

        loadContext = LoadContext.create(User.class)
                .setQuery(LoadContext.createQuery("select p from sec$User p where p.login = :username")
                        .setParameter("username", sender));
        User senderUser = dataManager.load(loadContext);

        if (senderUser != null) newMsg.setSender(senderUser);
        newMsg.setIsSystem(senderUser == null);
        newMsg.setRecipient(receiverUser);
        newMsg.setSubject(subject);
        newMsg.getMessageText().setText(text);

        dataManager.commit(newMsg);
    }

}